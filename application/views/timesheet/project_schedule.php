<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Project</a></li>
            <li class="active">Schedule</li>
            <li class="active"></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Project Schedule</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-stripped table-bordered" >
                            <tr align="center" class="success">
                                <td rowspan="3"> N o</td>
                                <td rowspan="3">Keterangan</td>                                
                                <td rowspan="3" colspan="2">Nama</td>
                                <td rowspan="3">Lokasi</td>
                                <td rowspan="3">Sche dule</td>
                                <td colspan="31">Aug-15</td>
                            </tr>
                            <tr align="center" class="success">
                                <td>1</td>
				                <td>2</td>
				                <td>3</td>
				                <td>4</td>
				                <td>5</td>
				                <td>6</td>
				                <td>7</td>
				                <td>8</td>
				                <td>9</td>
				                <td>10</td>
				                <td>11</td>
                                <td>12</td>
				                <td>13</td>
				                <td>14</td>
				                <td>15</td>
				                <td>16</td>
				                <td>17</td>
				                <td>18</td>
				                <td>19</td>
                                <td>20</td>
                                <td>21</td>
				                <td>22</td>
				                <td>23</td>
				                <td>24</td>
				                <td>25</td>
				                <td>26</td>
				                <td>27</td>
				                <td>28</td>
				                <td>29</td>
				                <td>30</td>
                                <td>31</td>
                            </tr>
                            <tr align="center" class="success">
                                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
                                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
                                <td></td>
                                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
				                <td></td>
                                <td></td>
                            </tr>
                            <tr align="center">
                            	<td rowspan="3" style="vertical-align:middle">1</td>
                                <td rowspan="3" style="vertical-align:middle">Rig Supt</td>
                                <td>1</td>
                                <td>Jakornad</td>
                                <td>V-01</td>
                                <td>28:28</td>
                                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td style="background-color: orange;">R</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
                                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
                                <td>O</td>
                                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Alexander Sirait</td>
                                <td>V-01</td>
                                <td>28:28</td>
                                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td style="background-color: orange;">T</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
                                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
                                <td>W</td>
                                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
                                <td>W</td>
                            </tr>   
							<tr align="center">
                                <td>3</td>
                                <td>Margono</td>
                                <td>V-01</td>
                                <td>28:28</td>
                                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td>O</td>
				                <td style="background-color: orange;">T</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
                                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
                                <td>W</td>
                                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
				                <td>W</td>
                                <td>W</td>
                            </tr>                            
                        </table>
                    </div>    
                </div>
            </div>
        </div>
    </section>
</div>