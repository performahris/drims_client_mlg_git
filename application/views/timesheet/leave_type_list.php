<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i> Leave</a></li>
            <li class="active">List Of Leave Type</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Leave | List Of Leave Type</h3>
                    </div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url(); ?>add/leave_type"><input type="button" class="btn btn-block btn-success" value="New"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                                <thead>
								  <tr>
									<th>No</th>
									<th>Code</th>
									<th>Name</th>
									<th>Allocation</th>
									<th>Action</th>
								  </tr>
								</thead>
								<tbody>
									<tr ">
										<td>1</td>
										<td>C_1</td>
										<td>Melahirkan</td>
										<td>90</td>
										<td class="action">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td>2</td>
										<td>C_2</td>
										<td>Kelahiran</td>
										<td>2</td>
										<td class="action">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
								</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
