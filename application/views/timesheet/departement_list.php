<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
           $(document).on("click", ".open-AddBookDialog", function () {
     var myBookId = $(this).data('id');
     $(".modal-body #departement_id").val( myBookId );
        });
          $(document).on("click", ".open-AddBookDialogs", function () {
     var myBookId = $(this).data('id');
     $(".modal-body #departement_name").val( myBookId );
        });
</script>
<script src="<?php echo base_url();?>asset/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>asset/plugins/datatables/dataTables.bootstrap.min.js"></script>

<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">DEPARTEMENT</a></li>
            <li class="active">Departement List</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">DEPARTEMENT | Departement List</h3>
                        <hr/>
                    </div>
				
					<!--<div class="col-md-6 col-xs-12 col-sm-12 pull-right" style="margin-top:20px;margin-bottom:20px;">-->
						<!--<div class="col-md-8">-->
            <div class="box-body" >
							<!--<div class="col-md-4 col-xs-12 col-sm-12 ">Search</div>-->
             
							<!--<div class="col-md-8 col-xs-12 col-sm-12 ">-->
              <!--<div class="input-group" style="width:200px; float: right;">
                <input type="text" class="form-control" placeholder="Search..." ng-model="search"/>
                 <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span>
              </div>-->
					<div class="row form-group">
                            <label for="searchby" class="col-md-3">Search By :</label>
                        </div>
						<?php echo form_open('dashboard/search_department');?>
							<div class="col-xs-12 col-sm-8 col-md-8">
								<div class="row form-group">
									<div class="col-xs-12 col-sm-5 col-md-5">
										<select class="form-control" name="search1">
											<option>-- Select Field Name --</option>
											<option value="departement_id">Dept.ID</option>
											<option value="departement_name">Name</option>
										</select>
									</div>
									<div class="col-xs-12 col-sm-2 col-md-1">
										<label for="code">Value</label>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<input type="text" class="form-control" name="val_search1"/>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-sm-5 col-md-5">
										<select class="form-control" name="search2">
											<option>-- Select Field Name --</option>
											<option value="departement_id">Dept.ID</option>
											<option value="departement_name">Name</option>
										</select>
									</div>
									<div class="col-xs-12 col-sm-1 col-md-1">
										<label for="code">Value</label>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<input type="text" class="form-control" name="val_search2"/>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-sm-5 col-md-5">
										<select class="form-control" name="search3">
											<option>-- Select Field Name --</option>
											<option value="departement_id">Dept.ID</option>
											<option value="departement_name">Name</option>
										</select>
									</div>
									<div class="col-xs-12 col-sm-1 col-md-1">
										<label for="code">Value</label>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<input type="text" class="form-control" name="val_search3"/>
									</div>
								</div>
							</div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                               
                                    <button type="submit" class="btn btn-info btn-block"><i class="fa fa-search"></i> Search</button>
                              
                            </div>
                        </div>
						
						<?php echo form_close()?>
                        <div class="col-md-12">&nbsp;</div>
                        <!-- END OF SEARCH -->
             
						</div>
					<!--</div>-->
          

                          <div class="col-md-6 col-xs-12 col-sm-12 pull-left" style="margin-top:20px;margin-bottom:20px;">
            <div class="col-md-4">
            <a href="<?php echo base_url('add/departement_register') ;?>"
              <button type="button" class="btn btn-success btn-block"><i class="fa fa-plus-circle"></i> Add</button></a>
            </div>
          </div>
                    <div class="box-body table-responsive " style="width:100%">
                        <table id="example" class="table table-bordered table-striped table-hover">
							<thead>
							  <tr class="success"> 
								<th class="text-center">No</th> 
								<th class="text-center">Dept. ID</th>
								<th class="text-center">
									<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
									Name
									<span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
									<span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
									</a>
								</th>
								<th class="text-center">Action</th>
							  </tr>	  
							</thead>
							<tbody>
              <!--<tr ng-repeat="dat in data | orderBy:sortType:sortReverse | filter:search| paging:limit:currentPage|offset: currentPage*itemsPerPage">-->
              <tr ng-repeat="dat in data | orderBy:sortType:sortReverse | filter:search | start: (currentPage - 1) * limit | limitTo: limit">
									<td class="text-center">{{($index+1)+((currentPage-1)*limit)}}</td>
									<td>{{dat.departement_id}}</td>
									<td>{{dat.departement_name}}</td>
									<td class="text-center">
                    <a href="" data-id="{{dat.departement_id}}" class="open-AddBookDialog button" data-toggle="tooltip" title="View Row" title="Row Detail"><i class="open-AddBookDialogs fa fa-search fa-lg" data-id="{{dat.departement_name}}"  data-toggle="modal" data-target="#myModal"></i></a>&nbsp;&nbsp;

                    <a href="<?php echo base_url('edit/departement_edit'); ?>/{{dat.departement_id}}"><i class="fa fa-pencil-square fa-lg" data-toggle="tooltip" title="Edit Row"></i></a>
                  </td> 
                  <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" style="height:30px; width:300px;" role="document">
                    <div class="modal-content" style="border-radius: 25px;">
                      <div class="">  
                        <h4 class="modal-title" id="myModalLabel" style="text-align: center; padding-top: 15px;"><b>Information Detail</b>
                      </div>
                      <div class="modal-body" style="width:340px; text-align: center;">
                        <div style="padding: 10px; text-align: center;">Dept.ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input style="border: 0;" id="departement_id" />
                        <br/>Dept.Name &nbsp;&nbsp;&nbsp;&nbsp;: <input style="border: 0;" id="departement_name" /></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" style="border-radius: 10px;" class="btn btn-default" data-dismiss="modal">OK</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal -->                  
                 </tr>   
              </tbody> 
                        </table>
              <!-- Pagination -->    
              <div ng-show="false">{{jumlah = (data | nama:search).length }}</div>                    
              <pagination total-items="(data | filter:search).length" items-per-page="limit" ng-model="currentPage " style="float: right;" max-size="5" class="pagination-sm "></pagination>
              <!--class="pagination-sm "-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $("a.button").click(function(){
		$.fancybox(
		'<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>dashboard/departement_detail" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
			{
				'autoDimensions'	: false,
				'width'         	: 'auto',
				'height'        	: 'auto',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			}
		);
	});
    });

    

    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {

      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =2;
      $scope.jumlah = 0;
      $scope.dataTables=true;

      $scope.data = <?php echo $departement; ?>
         
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.data.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.data.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };      
     
    });

    app.filter("paging", function() {      
      return function(items, limit,currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
     
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });

    app.filter('start', function () {
                return function (input, start,search) {
                    if (!input || !input.length) { return; }
                    if(search){ 
                    start = (+start);
                    return input.slice(start);
                  } else {
                   start = +start;
                    return input.slice(start); }
                };
       

 /* app.filter('offset', function() {
  return function(input, start) {
    start = parseInt(start, 10);
    return input.slice(start);
  }; */
});
 

</script>

