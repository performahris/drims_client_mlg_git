<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"> HR</a></li>
            <li class="active">List Man Power Request</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">HR | List Man Power Request</h3>
                    </div>
					<div class="col-md-6 col-xs-12 col-sm-12 pull-left" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url(); ?>/add/man_power_request"><input type="button" class="btn btn-block btn-success" value="New"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12 pull-right" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 ">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body  table-responsive" style="width:100%">
                        <table class="table table-bordered table-striped">
							<tr> 
								<th >No</th> 
								<th >Man Power Request Code</th>
								<th >Man Power Request Date</th>
								<th >Position</th>
								<th >
									<a href="#" ng-click="sortType = 'creator'; sortReverse = !sortReverse">
									Creator
									<span ng-show="sortType == 'creator' && !sortReverse" class="fa fa-caret-down"></span>
									<span ng-show="sortType == 'creator' && sortReverse" class="fa fa-caret-up"></span>
									</a>
								</th>
								<th >
									<a href="#" ng-click="sortType = 'dept'; sortReverse = !sortReverse">
									Departement
									<span ng-show="sortType == 'dept' && !sortReverse" class="fa fa-caret-down"></span>
									<span ng-show="sortType == 'dept' && sortReverse" class="fa fa-caret-up"></span>
									</a>
								</th>
								<th >Qty</th>
								<th>Status</th>
								<th >Action</th>
							  </tr>	
							<tr ng-repeat="m in mpr | orderBy:sortType:sortReverse | search:search">
								<td>{{$index+1}}</td>
								<td>{{m.id}}</td>
								<td>{{m.tanggal |date:'dd MMMM yyyy'}}</td>
								<td>{{m.posisi}}</td>
								<td>{{m.creator}}</td>
								<td>{{m.dept}}</td>
								<td>{{m.qty}}</td>
								<td>
										
									<span class="label label-success" ng-show="'{{m.status}}'=='closed'">Closed</span>
									<span class="label label-danger" ng-show="'{{m.status}}'=='open'">Open</span>
								</td>
								<td ><a ng-href="<?php echo base_url(); ?>dashboard/list_employee_on_mpr/{{m.id}}"> Detail</a></td>  
							</tr> 
                        </table>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.depart=""; 
      $scope.jumlah = 0;                         
      
      $scope.mpr = [
        { id: 'E1', name: 'xxx', dept: 'Research', creator: "Mr. A", status: 'open', tanggal : '2015-10-10', posisi: 'Staff', qty : '5'},
        { id: 'E2', name: 'yyy', dept: 'Research', creator: "Mr. A", status: 'open', tanggal : '2015-12-23', posisi: 'Staff', qty : '5'},
        { id: 'E3', name: 'qqq', dept: 'Research', creator: "Mr. B", status: 'open', tanggal : '2015-10-11', posisi: 'Staff', qty : '7'},
        { id: 'E4', name: 'www', dept: 'Research', creator: "Mr. D", status: 'open', tanggal : '2015-10-12', posisi: 'Staff', qty : '9'},
        { id: 'E5', name: 'eee', dept: 'Research', creator: "Mr. C", status: 'open', tanggal : '2015-10-13', posisi: 'Staff', qty : '5'},
        { id: 'E6', name: 'rrr', dept: 'Research', creator: "Mr. D", status: 'open', tanggal : '2015-10-18', posisi: 'Staff', qty : '10'},
        { id: 'E7', name: 'ttt', dept: 'HRD', creator: "Mr. A", status: 'open', tanggal : '2015-10-29', posisi: 'Staff', qty : '5'},
        { id: 'E8', name: 'uuu', dept: 'HRD', creator: "Mr. A", status: 'open', tanggal : '2015-10-01', posisi: 'Staff', qty : '90'},
        { id: 'E9', name: 'jjj', dept: 'Purchasing', creator: "Mr. A", status: 'open', tanggal : '2015-10-20', posisi: 'Staff', qty : '5'},
        { id: 'E10', name: 'mmm', dept: 'HRD', creator: "Mr. A", status: 'closed', tanggal : '2015-10-10', posisi: 'Staff', qty : '5'}
      ];
      
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.mpr.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.mpr.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
	
	app.filter("search", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].id.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

        
    

</script>
