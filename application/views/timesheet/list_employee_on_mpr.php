<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">MAN POWER REQUEST</a></li>
            <li class="active">Employee On Man PowerRequest</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">MAN POWER REQUEST | Employee On Man PowerRequest</h3>
                    </div>
					<div class="col-md-12 col-xs-12 col-sm-12" style="margin:20px;0px;20px;0px;">
						<div class="col-md-4">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
							<tr> 
								<th >No</th> 
								<th >Employee ID</th>
								<th >
									<a href="#" ng-click="sortType = 'creator'; sortReverse = !sortReverse">
									Employee Name
									<span ng-show="sortType == 'creator' && !sortReverse" class="fa fa-caret-down"></span>
									<span ng-show="sortType == 'creator' && sortReverse" class="fa fa-caret-up"></span>
									</a>
								</th>
								<th >
									<a href="#" ng-click="sortType = 'ongoing'; sortReverse = !sortReverse">
									Ongoing Project
									<span ng-show="sortType == 'ongoing' && !sortReverse" class="fa fa-caret-down"></span>
									<span ng-show="sortType == 'ongoing' && sortReverse" class="fa fa-caret-up"></span>
									</a>
								</th>
								<th >
									<a href="#" ng-click="sortType = 'nextproject'; sortReverse = !sortReverse">
									Next Project
									<span ng-show="sortType == 'nextproject' && !sortReverse" class="fa fa-caret-down"></span>
									<span ng-show="sortType == 'nextproject' && sortReverse" class="fa fa-caret-up"></span>
									</a>
								</th>
								<th >Status</th>
							</tr>	
							<tr ng-repeat="m in mpr | orderBy:sortType:sortReverse | filter:seacrh">
								<td>{{$index+1}}</td>
								<td>{{m.id}}</td>
								<td>{{m.name}}</td>
								<td>{{m.ongoing}}</td>
								<td>{{m.nextproject}}</td>
								<td>
									<span class="label label-success" ng-show="{{m.status}}==1"><i class="fa fa-check">&nbsp;</i>Assigned</span>
									<span class="label label-danger" ng-show="{{m.status}}==0"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>
								</td>  
							 </tr>  
                        </table>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.depart="";  
      $scope.jumlah = 0;                        
      
      $scope.mpr = [
        { id: 'E1', name: 'Danny', ongoing : 'PA', nextproject : 'PZ', status : 1},
        { id: 'E2', name: 'Erry', ongoing : 'PB', nextproject : 'PZ', status : 1},
        { id: 'E3', name: 'Tri', ongoing : 'PC', nextproject : 'PZ', status : 1},
        { id: 'E4', name: 'Handhika', ongoing : 'PA', nextproject : 'PZ', status : 1},
        { id: 'E5', name: 'Bemper', ongoing : 'PA', nextproject : 'PZ', status : 0},
        { id: 'E6', name: 'Kili', ongoing : 'PC', nextproject : 'PZ,PX', status : 1}
      ];
      
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.mpr.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.mpr.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

        
    

</script>
