<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i> LEAVE</a></li>
            <li class="active">List permission to go home quickly</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">LEAVE | List permission to go home quickly</h3>
                    </div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url(); ?>/add/ijin_pulang_cepat"><input type="button" class="btn btn-block btn-success" value="New"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered ">
                                <tr>
                                    <th rowspan = 2>No</th>
									<th rowspan = 2>Request Number</th>
									<th rowspan = 2>Date</th>
									<th rowspan = 2>Employee ID</th>
									<th rowspan = 2>Employee Name</th>
									<th rowspan = 2>Category</th>
									<th colspan = 2 >Time</th>
									<th rowspan = 2>Detail</th>
									<th rowspan = 2>Status</th>
                                </tr>
								<tr>
									<th>From</th>
									<th>To</th>
								</tr>
								<tr>
									<td>1</td>
									<td>1</td>
									<td>16-Feb-2016</td>
									<td>E_1</td>
									<td>Dery</td>
									<td>Pulang Cepat</td>
									<td>14.00</td>
									<td> - </td>
									<td class="action"><center>
									   <a href="#" class="button" title="Edit row"><i class="fa fa-search" style="font-size:20px;color:blue"></i></a>
									</center>
									</td>
									<td class="action">
									   <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Approve</span>
									</td>
                                </tr>
								<tr>
									<td>2</td>
									<td>1</td>
									<td>16-Feb-2016</td>
									<td>E_2</td>
									<td>Badrun</td>
									<td>Keluar</td>
									<td>10.00</td>
									<td>13.00</td>
									<td class="action" style="text-align:center">
									   <a href="#" class="button" title="Edit row"><i class="fa fa-search" style="font-size:20px;color:blue"></i></a>
									</td>
									<td class="action">
									   <span class="label label-danger"><i class="fa fa-remove">&nbsp;</i>Reject</span>
									</td>
								</tr>

								<tr>
									<td>3</td>
									<td>2</td>
									<td>17-Feb-2016</td>
									<td>E_2</td>
									<td>Badrun</td>
									<td>Keluar</td>
									<td>10.00</td>
									<td>13.00</td>
									<td class="action" style="text-align:center">
									   <a href="#" class="button" title="Edit row"><i class="fa fa-search" style="font-size:20px;color:blue"></i></a>
									</td>
									<td>
									   <a href="<?php echo base_url() . 'add/leave_approval' ?>" ><span class="label label-warning"><i class="fa fa-exclamation-triangle">&nbsp;</i>Need Approval</span></a>
									</td>
								</tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
