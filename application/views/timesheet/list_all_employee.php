<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i>HR</a></li>
            <li class="active">List All Employee</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					 <div class="box-header">
                        <h3 class="box-title">HR | List All Employee</h3>
                    </div>
					<div class="box-body">
						<div class="col-md-4 col-xs-12 col-sm-12">
								<div class="col-md-4 col-xs-12 col-sm-12 pull-left"> Employee Name</div> 
								<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
								</tr>
							</table>
						</div>
						
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Joined Date</div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">
								
                <table class="table">
								<tr>
									<td>
										From <input type="text" name="dari" class="form-control" ng-model="dari" id="datepicker2"/>
									</td>
								</tr>
								<tr>
									<td>
										To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" class="form-control" name="to" ng-model="to" id="datepicker1"/>
									</td>
								</tr>
								</table>
							</div>
						</div>

                <div class="col-md-6 col-xs-12 col-sm-12 pull-left" style="margin-top:20px;margin-bottom:20px;">
                  <div class="col-md-4">
                    <a href="<?php echo base_url(); ?>add/list_all_employee_register"><input type="button" class="btn btn-block btn-success" value="New"></a>
                  </div>
                </div>

						<div class="col-md-4 col-xs-12 col-sm-12">
							<select ng-model="depart" class="departemen" >
								<option value= "">Choose Departemen</option>
								<option value="{{ dept.nama }}" ng-repeat="dept in departemen">{{ dept.nama }}</option>
							</select> 
						</div>
								  
					</div>       
                    <div class="box-body table-responsive">
                        <table class="table-bordered table-striped table">
                                <tr>
                                    <th >No</th> 
									<th >Employee ID</th>
									<th >
										<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
										Emloyee Name
										<span ng-show="sortType == 'name' && !sortReverse"></span>
										<span ng-show="sortType == 'name' && sortReverse"></span>
										</a>
									</th>
									
                  <th >
                    <a href="#" ng-click="sortType = 'gender'; sortReverse = !sortReverse">
                    Gender
                    <span ng-show="sortType == 'gender' && !sortReverse"></span>
                    <span ng-show="sortType == 'gender' && sortReverse"></span>
                    </a>
                  </th>

                  <th >
                    <a href="#" ng-click="sortType = 'stat'; sortReverse = !sortReverse">
                    Status
                    <span ng-show="sortType == 'stat' && !sortReverse"></span>
                    <span ng-show="sortType == 'stat' && sortReverse"></span>
                    </a>
                  </th>

                  <th >
                    <a href="#" ng-click="sortType = 'address'; sortReverse = !sortReverse">
                    Address
                    <span ng-show="sortType == 'address' && !sortReverse"></span>
                    <span ng-show="sortType == 'address' && sortReverse"></span>
                    </a>
                  </th>

                  <th >
                    <a href="#" ng-click="sortType = 'phone'; sortReverse = !sortReverse">
                    Phone
                    <span ng-show="sortType == 'phone' && !sortReverse"></span>
                    <span ng-show="sortType == 'phone' && sortReverse"></span>
                    </a>
                  </th>

									<th >
										<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">
										Joined Date
										<span ng-show="sortType == 'join' && !sortReverse"></span>
										<span ng-show="sortType == 'join' && sortReverse"></span>
										</a>
									</th>

									<th><input type="checkbox" ng-model="selectAll" ng-click="checkAll()" /></th>
                                </tr>
								<tr ng-repeat="em in employee | orderBy:sortType:sortReverse ">
                                    <td>{{($index+1)+((currentPage-1)*limit)}}</td>
									<td>{{em.employee_id}}</td>
									<td>{{em.employee_name}}</td>
                  <td>{{em.employee_gender}}</td>
                  <td>{{em.employee_status}}</td>
                  <td>{{em.employee_address}}</td>
                  <td>{{em.employee_phone_number}}</td>
									
									<td>{{em.employee_date_of_birth |date:'dd MMMM yyyy'}}</td>
									
									<td><input type="checkbox" ng-model="em.Selected" /></td>
                                </tr>
                        </table>
						<div ng-show="false">{{jumlah = (employee | departemen:depart | nama:search | dateRange:dari:to ).length }}</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>
