<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">Man Power</a></li>
            <li class="active">Recuitment</li>
            <li class="active">Penarikan Karyawan</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">MAN POWER | Recuitment | Form Penarikan Karyawan Creation</h3><br>
                        <p><hr></p>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-12 pull-left">
                        <label for="comment">By         : (Auto Generate Div Head)</label>
                    </div>
                    <div class="box-body table-responsive " style="width:100%">
<thead>
        <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#80ff80;"><center>DATA PRIBADI</center></div>
                <div class="panel-body">
                    <pre>
<label for="comment">Nama Karyawan       : </label>
<label for="comment">Tempat/Tanggal Lahir: </label>
<label for="comment">Agama               : </label>
<label for="comment">Jenis Kelamin       : </label>
<label for="comment">Status Perkawinan   : </label>
<label for="comment">Alamat              : </label></pre>
                </div>

                <div class="panel-heading" style="background-color:#80ff80;"><center>DATA PENDIDIKAN DAN PENGALAMAN KERJA</center></div>
                <div class="panel-body">
                    <pre>
<label for="comment">1. Pendidikan Terakhir         :</label>
<label for="comment">2. Gaji & Tunjangan Terakhir    
    A. Gaji pokok              :
    B. Tunjangan Makan         :
    C. Tunjangan Transport     :
    D. Tunjangan Lain - Lain   :</label>
<label for="comment">3. Jabatan Terakhir            :</label>
<label for="comment">4. Masa Kerja dibidang YBS     :</label></pre>
                </div>

                <div class="panel-heading" style="background-color:#80ff80;"><center>ALASAN DIREKOMENDASIKAN</center></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color:#80ff80;"><center>DIVISI YANG BERSANGKUTAN</center></div>
                                <div class="panel-body">
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color:#80ff80;"><center>DIVISI YANG BERSANGKUTAN</center></div>
                                <div class="panel-body">
                                    <p><center>(AUTO GENERADE)</center></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading" style="background-color:#80ff80;"><center>DATA PENDIDIKAN DAN PENGALAMAN KERJA</center></div>
                <div class="panel-body">
<label for="comment">1. Jabatan                                 : </label><br> 
            <div class="col-md-4 col-xs-12 col-sm-12">
                        <input list="list_barang" class="form-control reset" 
                        placeholder="Isi id..." name="id_barang" id="id_barang" 
                        autocomplete="off" onchange="showBarang(this.value)">
                            <datalist id="list_barang">
                                <option>kaaka</option>
                            </datalist>
                    </div>                             
<label for="comment">2. Departemen / Divisi                     :</label>
<label for="comment">3. Status hubungan Kerja                   :</label>
<label for="comment">4. Lokasi Kerja                            :</label>
<label for="comment">5. Golongan                                :</label>
<label for="comment">6. Gaji Yang Diminta                       :</label>
<label for="comment">7. Tunjangan / Fasilitas yang diminta      :</label>
<label for="comment">8. Gaji yang diusulkan                     :</label>
<label for="comment">9. Tunjangan / Fasilitas yang disetujui    :</label>
<label for="comment">10. Gaji yang disetujui                    :</label>
<label for="comment">11. Mulai bekerja pada tanggal             :</label>
                </div>

            </div>
        </div>    
</thead>
                <div class="col-md-4 col-xs-12 col-sm-12" style="padding-top:1px;">
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="submit" class="btn btn-block btn-success" value="Save"></a>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $("a.button").click(function(){
		$.fancybox(
		'<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>dashboard/departement_detail" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
			{
				'autoDimensions'	: false,
				'width'         	: 'auto',
				'height'        	: 'auto',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			}
		);
	});
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.jumlah = 0;
                                          
      
      $scope.data = <?php echo $departement; ?>
         
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.data.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.data.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
     
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });

</script>
