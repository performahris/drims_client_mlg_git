<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"> HR</a></li>
            <li class="active">Man Power Request Release</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">HR | Man Power Request Release</h3>
                    </div>
					<div class="col-md-12 col-xs-12 col-sm-12" style="margin:20px;0px;20px;0px;">
						<div class="col-md-4">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-striped">
							<tr>
								<td width=20%>Release By</td>
								<td width=1%>:</td>
								<td >Dery</td>
							</tr>
							<tr>
								<td>Project</td>
								<td >:</td>
								<td >S</td>
							</tr>
							<tr>
								<td>Requestor</td>
								<td>:</td>
								<td>Danny</td>
							</tr>
							<tr>
								<td>Department</td>
								<td >:</td>
								<td >D1</td>
							</tr>
							<tr>
								<td>Date Request</td>
								<td >:</td>
								<td >30 Jan 2015</td>
							</tr>
							<tr>
								<td>Qty Request</td>
								<td >:</td>
								<td >5</td>
							</tr> 
                        </table>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
							  <tr> 
								<th >No</th> 
								<th >Employee ID</th>
								<th >Employee Name</th>
								<th >Departement</th>
								<th >
									Date Mutation
								</th>
								<th >
									Date End
								</th>
								<th >Status</th>
							  </tr>	  
							</thead>
							<tbody>
								<tr ng-repeat="m in mpr | orderBy:sortType:sortReverse | filter:seacrh">
									<td>{{$index+1}}</td>
									<td>{{m.id}}</td>
									<td>{{m.name}}</td>
									<td>{{m.dept}}</td>
									<td>{{m.mutation |date:'dd MMMM yyyy'}}</td>
									<td>{{m.mutation |date:'dd MMMM yyyy'}}</td>
									<td>Detail</td>  
								 </tr>  
							</tbody> 
                        </table>
						<br />
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
						<div ng-show="false">{{jumlah = (mpr | filter:seacrh).length }}</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.depart=""; 
      $scope.jumlah = 0;                         
      
      $scope.mpr = [
        { no: '1', id: 'E1', name: 'Danny', dept: 'AA', mutation: "2013-02-01", status: 'Single'},
        { no: '2', id: 'E2', name: 'Erry', dept: 'AB', mutation: "2013-02-01", status: 'Single'},
        { no: '3', id: 'E3', name: 'Tri', dept: 'BC', mutation: "2013-02-01", status: 'Single'},
        { no: '1', id: 'E1', name: 'Handhika', dept: 'AA', mutation: "2013-02-01", status: 'Single'},
        { no: '2', id: 'E2', name: 'Bemper', dept: 'AB', mutation: "2013-02-01", status: 'Single'},
        { no: '3', id: 'E3', name: 'Kili', dept: 'BC', mutation: "2013-02-01", status: 'Single'},
        { no: '1', id: 'E1', name: 'Rahasia', dept: 'AA', mutation: "2013-02-10", status: 'Single'},
        { no: '2', id: 'E2', name: 'Aku', dept: 'AB', mutation: "2013-03-01", status: 'Single'},
        { no: '3', id: 'E3', name: 'Kamu', dept: 'BC', mutation: "2013-02-01", status: 'Single'},
        { no: '4', id: 'E4', name: 'Dirinya', dept: 'BC', mutation: "2013-02-01", status: 'Jomblo'}
      ];
      
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.mpr.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.mpr.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

        
    

</script>
