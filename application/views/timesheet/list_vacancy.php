<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"> VACANCY</a></li>
            <li class="active">List Vacancy</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
					<div class="box-header">
                        <h3 class="box-title">VACANCY | List Vacancy</h3>
                    </div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url()."add/vacancy_register" ?>"><input type="button" class="btn btn-block btn-success" value="new"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body  table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Vancancy ID</th>
									<th>equest By</th>
									<th>Departement</th>
									<th>Vancancy Name</th>
									<th>Due Date</th>
									<th>Number of Request</th>
									<th >Action</th>
                                </tr>
								<tr>
                                    <td>Vacancy 1</td>
									<td>E_1</td>
									<td>HR</td>
									<td>Vacancy Trainer</td>
									<td>10 Feb 2016</td>
									<td>3</td>
									<td>
										<a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-search"></i></i></a>
										&nbsp;
										<a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
										&nbsp;
										<a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-trash-o"></i></i></a>
									</td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
