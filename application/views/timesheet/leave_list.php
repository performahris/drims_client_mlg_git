<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i> Leave</a></li>
            <li class="active">Leave List</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Leave | Leave List</h3>
                    </div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url(); ?>add/leave_type"><input type="button" class="btn btn-block btn-success" value="New"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered ">
                                <thead>
								  <tr>
									<th>No</th>
									<th>Request Number</th>
									<th>Date</th>
									<th>Employee ID</th>
									<th>Employee Name</th>
									<th>Sisa Cuti</th>
									<th>Ambil Cuti</th>
									<th>Detail</th>
									<th>status</th>
								  </tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>C_1</td>
										<td>1 Jan 2009</td>
										<td>E_1</td>
										<td>Dery</td>
										<td>8</td>
										<td>5</td>
										<td class="action">
										   <i class="fa fa-search"></i>
										</td>
										<td><span class="label label-success">Approved</span></td>
									</tr>
									<tr>
										<td>1</td>
										<td>C_2</td>
										<td>1 Jan 2009</td>
										<td>E_1</td>
										<td>Dery</td>
										<td>8</td>
										<td>5</td>
										<td class="action">
										   <i class="fa fa-search"></i>
										</td>
										<td><span class="label label-danger">Rejected</span></td>
									</tr>
									<tr>
										<td>1</td>
										<td>C_3</td>
										<td>1 Jan 2009</td>
										<td>E_1</td>
										<td>Dery</td>
										<td>8</td>
										<td>5</td>
										<td class="action">
										   <i class="fa fa-search"></i>
										</td>
										<td><a href="<?php echo base_url()."edit/leave_approval"  ?>"><span class="label label-primary">Need Approval</span></a></td>
									</tr>
								</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
