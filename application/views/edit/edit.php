<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Asset</a></li>
            <li class="active">List & Tracking</li>
        </ol>
    </section>
	<div class="box-body">
		<div class="col-sm-4">
			<table class="table table-condensed">
				<tr valign="middle">
					<td>Employee Name</td>
					<td>:</td>
					<td><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></td>
				</tr>
				<tr></tr>
				<tr></tr>
			</table>
		</div>
    </div>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Asset | List & Tracking</h3>
                    </div>
                    <div class="box-body">
                        <select ng-model="depart" class="departemen">
							<option value= "">Choose Departemen</option>
							<option ng-repeat="dept in departemen" value="{{ dept.nama }}">{{ dept.nama }}</option>
						</select> 
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>No</th>
									<th>No SPPD</th>
									<th>Name</th>
									<th>Division</th>
									<th>Date</th>
									<th>Status</th>
									<th >Action</th>
                                </tr>
								<tr>
                                    <td>1</td>
									<td>SPPD_1</td>
									<td>Lorem Ipsum</td>
									<td>Production</td>
									<td>10 Feb 2016</td>
									<td>Rejected</td>
									<td>
										<i class="fa fa-search">
										&nbsp;
										<a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
									</td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
