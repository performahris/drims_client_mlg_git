<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">SPDD</a></li>
            <li class="active">SPPD Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
               <div class="box box-primary">
                   <div class="box-header">
                        <h3 class="box-title">SPPD | SPPD Edit</h3>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>
								<div class="col-md-12 col-xs-12 col-sm-12">No</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									(Auto Generate)
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="col-md-3 col-xs-12 col-sm-12">Date</div>
									<div class="col-md-9 col-xs-12 col-sm-12" style="font-weight:normal">
										<?php echo date("d F Y")?>
									</div>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Nama</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="nama" class="form-control"  placeholder ="Name"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Divisi</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="divisi" class="form-control"  placeholder ="Divisi"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Tujuan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-10 col-xs-12 col-sm-12" style="font-weight:normal">
									<textarea name="tujuan" rows="5" class="form-control" >	</textarea>
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Periode</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="periode" class="form-control"  placeholder ="Periode"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Sarana Transportasi Perjalanan Dinas</strong></h4>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="radio" name="sarana" /> Pesawat
								</div>
							</label>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="radio" name="sarana" /> Kereta Api / Bus / Kapal Laut
								</div>
							</label>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12">
									Kelas
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="kereta" class="form-control"/>
								</div>
							</label>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="radio" name="sarana" /> Mobil Operasional Perusahaan
								</div>
							</label>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12">
									No Polisi
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style	="font-weight:normal">
									<select name="name">
										<option>B 3 MO</option>
										<option>B 3 CAK</option>
									</select>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12"><input type="radio" name="sarana" /> Lain - Lain</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="lainlain" class="form-control"  placeholder ="Lain - Lain"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Kas Bon Yang Diajukan</strong></h4>
						</div>
						<table id="example1" class="table table-striped" style="width:50%">
							<tr>
								<td>Uang Makan</td>
								<td>IDR</td>
								<td >Lorem Ipsum</td>
							</tr>
							<tr>
								<td>Hotel</td>
								<td>IDR</td>
								<td>Lorem Ipsum</td>
							</tr>
							<tr>
								<td>Transportasi / Taksi P.P</td>
								<td>IDR</td>
								<td >Lorem Ipsum</td>
							</tr>
							<tr>
								<td>Lain - Lain</td>
								<td>IDR</td>
								<td>Lorem Ipsum</td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td><strong>Total</strong></td>
								<td><strong>IDR</strong></td>
								<td ><strong>(auto generate)</strong></td>
							</tr>
							<tr>
								<td>Remaks</td>
								<td colspan=2><textarea name="remaks" rows="10" style="width:100%"></textarea>
							</tr>
						</table>
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Approve"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Reject"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
