<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">EMPLOYEE</a></li>
            <li class="active">Approval Request</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">DEPARTEMENT | Departement Register</h3>
                    </div>
					<div class="col-md-6 col-xs-12 col-sm-12" >
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Date Request</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><?php echo date("d F Y"); ?></div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
						
						<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Tittle Needed</div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">Lorem Ipsum</div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Qty</div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">xx</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" >
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Departement</div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">Lorem Ipsum</div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Location</div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">Lorem Ipsum</div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Direct Supervisior</div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">Lorem Ipsum</div>
						</div>
					</div>
					<div>&nbsp;</div>
					<div class="box-header" ><h4>ALASAN UNTUK MEREKRUT</h4></div>
                    <div class="box-body">
						<div class="col-md-3 col-xs-12 col-sm-12" >
							<input type="checkbox" name="alasan" value="Posisi Baru"/> Posisi Baru
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12" >
							<input type="checkbox" name="alasan" value="Tambahan Karyawan"/> Tambahan Karyawan
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12" >
							<input type="checkbox" name="alasan" value="Penggantian (Replacement)"/> Penggantian (Replacement)
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12" style="padding-bottom:10px">
							<input type="checkbox" name="alasan" value="Perpanjangan Kontrak"/> Perpanjangan Kontrak
						</div>
						<div class="box-header" >
							<h4>PERTIMBANGAN UNTUK MEREKRUT</h4>
							(Jelaskan mengapa posisi tersebut perlu ditambah dan bagaimana posisi tersebut memberikan kontribusi dalam meningkatkan produktivity / profitability)
						</div>
						Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
						<div class="box-header" >
							<h4>RINGKASAN DAN TANGGUNGJAWAB PEKERJAAN</h4>
						</div>
						Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
						<div class="box-header" >
							<h4>GAMBARKAN STRUKTUR ORGANISASI TERAKHIR DEPARTEMEN TERSEBUT DAN POSISI CALON KARYAWAN</h4>
						</div>
						Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
						<div>&nbsp;</div>
						<div class="box-header" ><h4>JENIS REKRUTMEN</h4></div>
						<div class="table-responsive">
							<table id="example1" class="table table-striped">
								<tr>
									<th style="padding-left:30px;">
										Kandidat Dari Luar
										
									</th>
									<th style="padding-left:30px;">
										Periode
									   
									</th>
									<th style="padding-left:30px;">
										Kandidat Dari Dalam
										
									</th>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Tetap"/> Tetap</td>
									<td style="padding-left:30px;">Lorem Ipsum</td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Mutasi"/> Mutasi</td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Darurat"/> Darurat</td>
									<td style="padding-left:30px;">Lorem Ipsum</td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Penugasan"/> Penugasan</td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Kontrak"/> Kontrak</td>
									<td style="padding-left:30px;">Lorem Ipsum</td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Lain-Lain"/> Lain - Lain</td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Lain-Lain"/> Lain - Lain</td>
									<td style="padding-left:30px;">Lorem Ipsum</td>
									<td style="padding-left:30px;">&nbsp;</td>
								</tr>
							</table>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12" >
							<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Jenis Kelamin</div>
								<div class="col-md-8 col-xs-12 col-sm-12 ">
									Lorem Ipsum
								</div>
							</div>
							<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Pendidikan</div>
									<div class="col-md-8 col-xs-12 col-sm-12 ">
										Lorem Ipsum
									</div>
								</div>
							<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Keahlian yang dibutuhkan</div>
								<div class="col-md-8 col-xs-12 col-sm-12 ">Lorem Ipsum</div>
							</div>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12" >
							<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Pengalaman yang dibutuhkan</div>
								<div class="col-md-8 col-xs-12 col-sm-12 ">
									Lorem Ipsum
								</div>
							</div>
							<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Usia</div>
								<div class="col-md-8 col-xs-12 col-sm-12 ">xx Tahun</div>
							</div>
							<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Kondisi Lingkungan Kerja</div>
								<div class="col-md-8 col-xs-12 col-sm-12 ">Lorem Ipsum</div>
							</div>
						</div>
						<div class="box-header" ><h5>DIDALAM DAN DILUAR KANTOR</h5></div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-2 col-xs-12 col-sm-12 pull-left">Lain - lain</div>
							<div class="col-md-4 col-xs-12 col-sm-12 ">Lorem Ipsum</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12" >
							<div class="col-md-4" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-12 col-xs-12 col-sm-12 pull-left">Kandidat Yang Diusulkan 
									Lorem Ipsum
								</div>
							</div>
							<div class="col-md-4" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-12 col-xs-12 col-sm-12 pull-left">PT 
									Lorem Ipsum
								</div>
							</div>
							<div class="col-md-4" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-12 col-xs-12 col-sm-12 pull-left">Departement 
									Lorem Ipsum
								</div>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12" style="padding-top:20px">
							<div class="col-md-2 col-xs-12 col-sm-12 pull-left">Remaks</div>
							<div class="col-md-10 col-xs-12 col-sm-12 pull-left">
								<textarea name="remaks" rows= "8" style="width:80%"></textarea>
							</div>
						</div>
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Approve"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Reject"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Cancel"></a>
							</div>
						</div>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

