<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">MAN POWER</a></li>
            <li class="active">Man Power Request</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">MAN POWER | Man Power Request</h3>
                    </div>

					<div class="col-md-6 col-xs-12 col-sm-12" >
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Date Request</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><?php echo date("d F Y"); ?></div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
						
						<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Tittle Needed</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="tittle" ng-model="tittle"/></div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Qty</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="qty" ng-model="qty"/></div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" >
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Departement</div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">
								<select name="departement">
									<option>Production</option>
									<option>Logistic</option>
								</select>
							</div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Location</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="tittle" ng-model="tittle"/></div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Direct Supervisior</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="qty" ng-model="qty"/></div>
						</div>
					</div>
					<div>&nbsp;</div>
					<div class="box-header" ><h4>ALASAN UNTUK MEREKRUT</h4></div>
                    <div class="box-body">
						<div class="col-md-3 col-xs-12 col-sm-12 pull-left">
							<input type="checkbox" name="posisi" id="optionsRadios1" value="Posisi Baru"> <label for="optionsRadios1">Posisi Baru</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12 pull-left">
							<input type="checkbox" name="posisi" id="optionsRadios2" value="Tambahan Karyawan">  <label for="optionsRadios2">Tambahan Karyawan</label>
						</div>	
						<div class="col-md-3 col-xs-12 col-sm-12 pull-left">
							<input type="checkbox" name="posisi" id="optionsRadios3" value="Penggantian (Replacement)">  <label for="optionsRadios3">Penggantian (Replacement)</label>
						</div>
						<div style="padding-left:15px;">
							<input type="checkbox" name="posisi" id="optionsRadios4" value="Perpanjangan Kontrak">  <label for="optionsRadios4">Perpanjangan Kontrak</label>
						</div>
						<div class="box-header" >
							<h4>PERTIMBANGAN UNTUK MEREKRUT</h4>
							(Jelaskan mengapa posisi tersebut perlu ditambah dan bagaimana posisi tersebut memberikan kontribusi dalam meningkatkan produktivity / profitability)
						</div>
						<textarea name="alasan" rows= "8" style="width:80%" class="form-control"></textarea>
						<div class="box-header" >
							<h4>RINGKASAN DAN TANGGUNGJAWAB PEKERJAAN</h4>
						</div>
						<textarea name="ringkasan" rows= "8" style="width:80%" class="form-control"></textarea>
						<div class="box-header" >
							<h4>GAMBARKAN STRUKTUR ORGANISASI TERAKHIR DEPARTEMEN TERSEBUT DAN POSISI CALON KARYAWAN</h4>
						</div>
						<textarea name="struktur_organisasi"  rows= "8" style="width:80%" class="form-control"></textarea>
						<div>&nbsp;</div>
						<div class="box-header" ><h4>JENIS REKRUTMEN</h4></div>
						<div class="table-responsive">
							<table id="example1" class="table table-striped">
								<tr>
									<th style="padding-left:30px;">
										Kandidat Dari Luar
										
									</th>
									<th style="padding-left:30px;text-align:center" >
										Periode
									   
									</th>
									<th style="padding-left:30px;">
										Kandidat Dari Dalam
										
									</th>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Tetap"/> <b>Tetap</b></td>
									<td style="padding-left:30px;"><input type="text" name="periode1" class="form-control" /></td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Mutasi"/> <b>Mutasi</b></td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Darurat"/> <b>Darurat</b></td>
									<td style="padding-left:30px;"><input type="text" name="periode2" class="form-control"/></td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Penugasan"/> <b>Penugasan</b></td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Kontrak"/> <b>Kontrak</b></td>
									<td style="padding-left:30px;"><input type="text" name="periode3" class="form-control"/></td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Lain-Lain"/> <b>Lain - Lain</b></td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Lain-Lain"/> <b>Lain - Lain</b></td>
									<td style="padding-left:30px;"><input type="text" name="periode4" class="form-control" /></td>
									<td style="padding-left:30px;">&nbsp;</td>
								</tr>
							</table>
						</div>
						<div class="col-md-5 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								Jenis Kelamin
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<select name="jenis_kelamin" class="form-control">
									<option>Pria</option>
									<option>Wanita</option>
								</select>
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								Pengalaman Yang Dibutuhkan
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<input type="text" class="form-control" name="pengalaman	" />
							</div>
						</div>
						<div class="col-md-5 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								Penidikan
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<select name="pendidikan" class="form-control">
									<option>SMP</option>
									<option>SMA</option>
									<option>S1</option>
									<option>S2</option>
								</select>
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								Usia
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								
								  
								  <div class="input-group">
									<input type="text" class="form-control"/>
									<div class="input-group-addon">
									  Tahun
									</div>
								  </div><!-- /.input group -->
								
							</div>
						</div>
						<div class="col-md-5 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								Keahlian Yang Dibutuhkan
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<input type="text" name="keahlian" class="form-control"/>
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								Kondisi Lingkungan Kerja
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">								
								 <input type="text" name="kondisi" class="form-control"/>
							</div>
						</div>
						<div class="box-header" ><h4>Didalam dan diluar kantor</h4></div>
						<div class="col-md-12 col-xs-12 col-sm-12" >
							<div class="col-md-2" style="margin-top:5px;margin-bottom:5px;">
								Lain Lain
							</div>
							<div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
								<input type="text" name="lainlain" class="form-control"/>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12" >
							<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-6 col-sm-6">
									Kandidat Yang Diusulkan 
								</div>
								<div class="col-md-6 col-xs-6 col-sm-6">
										<select name="kandidat" class="form-control">
											<option>Dery</option>
											<option>Badrun</option>
										</select>
								</div>
							</div>
							<div class="col-md-3 col-xs-6 col-sm-6" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12">
									PT
								</div>
								<div class="col-md-6 col-xs-6 col-sm-6">
									<select name="pt" class="form-control">
										<option>Batavianet</option>
										<option>G-Media</option>
									</select>
								</div>
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
								<div class="col-md-4 col-xs-12 col-sm-12">
									Departement
								</div>
								<div class="col-md-6 col-xs-6 col-sm-6">
									<select name="dept" class="form-control">
										<option>Produksi</option>
										<option>HRD</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Save as Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

