<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">DEPARTMENT</a></li>
            <li class="active">Departement Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">DEPARTMENT | Departement Edit</h3>
                    </div>
					<?php echo form_open('edit/departement_edit_action');?>
						<div class="box-body table-responsive">
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>ID Departemen</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label><input type="text" name="id" class="form-control" value="<?php echo $data->departement_id; ?>" required />
                                <input type="hidden" value="<?php echo $data->departement_id; ?>" name="kode" >
                                </label>
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>Nama Departemen</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label><input type="text" name="name" class="form-control" value="<?php echo $data->departement_name; ?>" required/></label>
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<input type="submit" class="btn btn-block btn-success" value="Save">
							</div>
						</div>
					<?php echo form_close()?>
                </div>
            </div>
        </div>
    </section>
</div>

