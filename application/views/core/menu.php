<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">&nbsp;</li>
            <li class="treeview <?php if($this->uri->segment(2)=="dash"){echo "active";} ?> ">
                <a href="<?php echo base_url('dashboard/dash') ?>">
                    <i class="fa fa-th"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)=="department_list"){echo "active";} ?> ">
                <a href="<?php echo base_url('dashboard/department_list'); ?>">
                    <i class="fa fa-th"></i> <span>Department</span>
                </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)=="list_man_power_request"){echo "active";} ?> ">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Man Power</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if($this->uri->segment(2)=="list_man_power_request"){echo "active";} ?>"><a href="<?php echo base_url('dashboard/list_man_power_request');?>"><i class="fa fa-circle-o"></i> List Man Power</a></li>
                    <li>
                        <a href="#">
                            <i class="fa fa-dashboard"></i><span>Recuitment</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('dashboard/interview_record');?>"><i class="fa fa-circle-o"></i> Interview Record</a></li>
                            <li><a href="<?php echo base_url('dashboard/pen_karyawan'); ?>"><i class="fa fa-circle-o"></i> Penarikan Karyawan</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Applicant</a></li>
                    <li class="<?php if($this->uri->segment(2)=="list_vacancy"){echo "active";} ?>"><a href="<?php echo base_url('dashboard/list_vacancy');?>"><i class="fa fa-circle-o"></i> Vacancy</a></li>
                    <li class="<?php if($this->uri->segment(2)=="man_power_request_release"){echo "active";} ?>"><a href="<?php echo base_url('dashboard/man_power_request_release');?>"><i class="fa fa-circle-o"></i> Man Power Release</a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url('dashboard/get_all_employee'); ?>">
                    <i class="fa fa-th"></i> <span>Employee</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-th"></i> <span>Employee Grade</span>
                </a>
            </li>            
            <li>
                <a href="#">
                    <i class="fa fa-th"></i> <span>Personnel Schedule</span>
                </a>
            </li>            
            <li>
                <a href="#">
                    <i class="fa fa-th"></i> <span>Bonus Sheet</span>
                </a>
            </li>            
            <li>
                <a href="<?php echo base_url('dashboard/ijin_list'); ?>">
                    <i class="fa fa-th"></i> <span>Ijin</span>
                </a>
            </li>            
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Leave Master</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('dashboard/leave_type_list');?>"><i class="fa fa-circle-o"></i> Leave Type</a></li>
                    <li><a href="<?php echo base_url('dashboard/leave_list');?>"><i class="fa fa-circle-o"></i> List Leave </a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url('dashboard/sppd_list'); ?>">
                    <i class="fa fa-th"></i> <span>SPPD</span>
                </a>
            </li>                        
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Tunjangan Master</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('dashboard/list_tunjangan');?>"><i class="fa fa-circle-o"></i> Tunjangan</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Tunjangan Dinas Luar Kota</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>