<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">ALLOWANCE</a></li>
            <li class="active">Allowance Creation</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">ALLOWANCE | Allowance Creation</h3>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Category</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<select name="category" class="form-control">
										<option>Lorem</option>
										<option>Ipsum</option>
									</select>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Position</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<select name="position" class="form-control">
										<option>Lorem</option>
										<option>Ipsum</option>
									</select>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Value</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="value" class="form-control"/>
								</div>
							</label>		
						</div>
						<!--
                        <table id="example1" class="table table-striped	 ">
							<tr>
								<td>Category</td>
								<td>:</td>
								<td>
									<select name="category">
										<option>Lorem</option>
										<option>Ipsum</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Position</td>
								<td>:</td>
								<td>
									<select name="position">
										<option>Lorem</option>
										<option>Ipsum</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Value</td>
								<td>:</td>
								<td><input type="text" name="value"/></td>
							</tr>
                        </table>
						-->
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Save as Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
