<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Created on Aug 26, 2011 by Damiano Venturin @ Squadra Informatica

class Dashboard extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		
		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array('server' => 'http://localhost:888/drims_server/index.php/hr_dashboard/'));
	}
	
	function _view( $template = '', $param = '')
	{
        $this->load->view('core/header');
        $this->load->view('core/menu');
        $this->load->view($template, $param);
        $this->load->view('core/footer');
    }

	public function get_all_employee(){
		$data['employee'] =json_encode($this->rest->get('all_employee'));
		$this->_view('timesheet/list_all_employee', $data);
	}
	
	public function departement_list(){
		
		//var_dump($search_a1, $search_a2, $search_b1, $operator_and, $operator_or); die();
		
		$data['departement'] = json_encode($this->rest->get('departement_list'));
		$this->_view('timesheet/departement_list', $data);
	}
	
	function search_department()
	{
		$search1 = $this->input->post('search1');
		$search2 = $this->input->post('search2');
		$search3 = $this->input->post('search3');
		$operator = $this->input->post('operator');
		$val1 = $this->input->post('val_search1');
		$val2 = $this->input->post('val_search2');
		$val3 = $this->input->post('val_search3');
		$query="select * from departement";
		
		for($i=1;$i<3;$i++){
			$variable = 'search';
			$value = 'val_search';
			if(!is_null($this->input->post($variable.$i))){
				if($this->input->post($variable.$i) != "-- Select Field Name --"){
						$query = $query." where ".$this->input->post($variable.$i)." like '%".$this->input->post($value.$i)."%' ";
				}
			}
		}
		
		$encode_que = base64_encode($query);
		$send = str_replace('/', '~',str_replace('+', '.', (str_replace('=', '-', $encode_que))));
		
		//var_dump($encode_que,base64_decode($encode_que)); die();
		$data['departement'] = json_encode($this->rest->get('search_departement_list/que/'.$send));
		
		
		$this->_view('timesheet/departement_list', $data);
	}
	
	function search_department_list()
	{
		$id=$this->input->post('id');
		$data1['data'] = $this->rest->post('search_department_list/svaha/1/format/php',$id);
		if($data1)
		{
			redirect('dashboard/result_departement_list/'.$id);
		}
		
	}
	function result_departement_list($id)
	{
		$data['departement'] = json_encode($this->rest->get('result_departement_list/id/'.$id));
		$this->_view('timesheet/departement_list', $data);
		//var_dump($data);
	}
	
		public function index()
	{
		$this->departement_list();
	}
	
	
	 function index2()
    {
        $this->_view('dashboard');
    }
    
    function dash()
    {
    	$this->_view('index1');
    }
    
    
    
    
    function list_all_employee()
    {
       $this->get_all_employee();
    }
	
	function leave_request_summary()
    {
        $this->_view('timesheet/leave_request_sumary');
    }
	
	function sppd_list()
    {
		$this->_view('timesheet/sppd_list');
	}
	
	function list_employee_request()
    {
		$this->_view('timesheet/list_employee_request');
	}
	
	function ijin_list()
    {
		$this->_view('timesheet/ijin_pulang_cepat_list');
	}
	
	function ijin_pulang_cepat_approval()
    {
		$this->_view('timesheet/ijin_pulang_cepat_approval');
	}
	
	function list_man_power_request()
    {
		$this->_view('timesheet/list_man_power_request');
	}
	
	function list_employee_on_mpr()
    {
		$this->_view('timesheet/list_employee_on_mpr');
	}
	
	function man_power_request_release()
    {
		$this->_view('timesheet/man_power_request_release');
	}
	
	function department_list()
    {
		$this->departement_list();
	}
	
	function list_of_employee_request()
    {
		$this->_view('timesheet/list_of_employee_request');
	}
	
	function leave_type_list()
	{
		$this->_view('timesheet/leave_type_list');
	}
	
	function leave_list()
	{
		$this->_view('timesheet/leave_list');
	}
	
	function leave_list_per_employee()
	{
		$this->_view('timesheet/leave_list_per_employee');
	}
	
	function list_tunjangan()
	{
		$this->_view('timesheet/list_tunjangan');
	}
	
	function list_vacancy()
	{
		$this->_view('timesheet/list_vacancy');
	}
	function interview_record()
    {
		$this->_view('timesheet/Interview_Record');
    }
    function pen_karyawan()
    {
		$this->_view('timesheet/penarikan_karyawan');
    }	

}
