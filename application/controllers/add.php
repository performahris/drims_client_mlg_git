<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();

		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array('server' => 'http://localhost:888/drims_server/hr_add/'));
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('core/header');
        $this->load->view('core/menu');
        $this->load->view($template, $param);
        $this->load->view('core/footer');
    }
	
	/*function ijin_approval () {
		$this->_view('add/ijin_approval');
	}*/
	
	function departement_register()
	{
		$this->_view('add/departement_register');
	}
	
	public function departement_register_add(){
		$data = array(
			'departement_id' => $this->input->post('departement_id'),
			'departement_name' => $this->input->post('departement_name')
		);
		
		$query = $this->rest->post('departement_register_add/svaha/1/format/php', $data);
		if($query) {
			redirect('dashboard/departement_list');
		} else {
			echo "<script>alert('Terjadi Error Saat Query')</script>";
		}
	}
	
	function list_all_employee_register()
	{
		$this->_view('add/list_all_employe_register');
	}
	
	public function list_all_employee_add(){
		$data = array(
			'departement_id' => $this->input->post('departement_id'),
			'departement_name' => $this->input->post('departement_name')
		);
		
		$query = $this->rest->post('departement_register_add/svaha/1/format/php', $data);
		if($query) {
			redirect('dashboard/departement_list');
		} else {
			echo "<script>alert('Terjadi Error Saat Query')</script>";
		}
	}
	/*
	function man_power_request()
	{
		$this->_view('add/man_power_request');
	}
	
	function ijin_pulang_cepat()
	{
		$this->_view('add/ijin_pulang_cepat');
	}
	
	function leave_type()
	{
		$this->_view('add/leave_type');
	}
	
	function leave_request()
	{
		$this->_view('add/leave_request');
	}
	
	function sppd_creation()
	{
		$this->_view('add/sppd_creation');
	}
	
	function tunjangan_creation()
	{
		$this->_view('add/tunjangan_creation');
	}
	
	function vacancy_register()
	{
		$this->_view('add/vacancy_register');
	}*/
}