<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();

		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array('server' => 'http://localhost:888/drims_server/hr_edit/'));
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('core/header');
        $this->load->view('core/menu');
        $this->load->view($template, $param);
        $this->load->view('core/footer');
    }
    
	function departement_edit($id)
    {
		$data['data'] = $this->rest->get('edit_departement/id/'.$id);
		$this->_view('edit/departement_edit',$data);
	}
	
	public function departement_edit_action(){
		$id = $this->input->post('kode');
		$data = array(
			'departement_id' => $this->input->post('id'), 'departement_name' => $this->input->post('name')
		);
		$query = $this->rest->post('edit_departement/id/'.$id.'/format/php', $data);
		if($query){
			redirect('dashboard/departement_list');
		} else {
			echo "<script>alert('Gagal coy'); window.close ();</script>";
		}
	}
	function departement_priview($id)
    {
		$data['data'] = $this->rest->get('edit_departement/id/'.$id);
		$this->_view('timesheet/departement_list',$data);
	}
}